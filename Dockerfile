# This file is a template, and might need editing before it works on your project.
# This Dockerfile installs a compiled binary into a bare system.
# You must either commit your compiled binary into source control (not recommended)
# or build the binary first as part of a CI/CD pipeline.

FROM benbruers/cern-root-ubuntu:v6-14-08

Add . /code/

WORKDIR /code/

RUN apt-get update && apt-get -y install texlive-full python-pip dvipng

RUN pip install --upgrade pip && pip2 install numpy==1.15.1 scipy==1.1.0 numexpr==2.6.8 matplotlib==2.2.3

#ENV ROOTSYS /usr/local
ENV MANPATH /usr/local/man
ENV LIBPATH /usr/local/lib
ENV PATH /usr/local/bin:/usr/local/sbin:/usr/sbin:/usr/bin:/sbin:/bin
ENV PYTHONPATH /usr/local/lib
ENV SHLIB_PATH /usr/local/lib
    
RUN  echo "#! /bin/bash" > /code/run_fd.sh && echo "source /usr/local/bin/thisroot.sh" >> /code/run_fd.sh &&\
    echo "source /code/functional-decomposition/bin/setup_bash.sh">> /code/run_fd.sh &&\
    echo "fd_scan.py" >> /code/run_fd.sh

#ENTRYPOINT ["/bin/bash", "/code/run_fd.sh"]

CMD /bin/bash /code/run_fd.sh
